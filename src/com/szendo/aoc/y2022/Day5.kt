package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val (startingState, moves) = Helper.getResourceAsStream("2022/5.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("") }

    val rows = startingState.dropLast(1)
        .map { line -> line.chunked(4).map { it[1] } }
        .reversed()

    val inputStacks = (0 until rows[0].size).map { i -> rows.map { row -> row[i] }.takeWhile { it != ' ' } }

    fun moveCrates(stacks: List<List<Char>>, moves: List<String>, reverseOrder: Boolean = true): List<List<Char>> {
        val result = stacks.toMutableList()

        for (move in moves) {
            val parts = move.split(' ')
            val count = parts[1].toInt()
            val from = parts[3].toInt() - 1
            val to = parts[5].toInt() - 1

            val cratesToMove = result[from].takeLast(count).let { if (reverseOrder) it.reversed() else it }
            result[from] = result[from].dropLast(count)
            result[to] = result[to] + cratesToMove
        }

        return result
    }

    fun getTopCrates(stacks: List<List<Char>>) = stacks.map { it.last() }.joinToString("")

    val answer1 = getTopCrates(moveCrates(inputStacks, moves))
    println("Part 1: $answer1")

    val answer2 = getTopCrates(moveCrates(inputStacks, moves, reverseOrder = false))
    println("Part 2: $answer2")
}
