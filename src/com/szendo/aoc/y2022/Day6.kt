package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2022/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val answer1 = input.windowed(4).withIndex().first { (_, value) -> value.toSet().size == value.length }.index + 4
    println("Part 1: $answer1")

    val answer2 = input.windowed(14).withIndex().first { (_, value) -> value.toSet().size == value.length }.index + 14
    println("Part 2: $answer2")
}
