package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    fun getScore(opponent: Char, own: Char): Int {
        val oppNum = opponent - 'A';
        val ownNum = own - 'X'
        val shapeScore = ownNum + 1;
        val outcomeScore = when {
            oppNum == ownNum -> 3
            (oppNum + 1) % 3 == ownNum -> 6
            else -> 0
        }
        return shapeScore + outcomeScore
    }

    fun getOwnShape(opponent: Char, outcome: Char): Char {
        val oppNum = opponent - 'A';
        val outcomeNum = outcome - 'X' - 1;
        val ownNum = (oppNum + 3 + outcomeNum) % 3;
        return 'X' + ownNum;
    }

    val input = Helper.getResourceAsStream("2022/2.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val answer1 = input.sumOf { getScore(it[0], it[2]) }
    println("Part 1: $answer1")

    val answer2 = input.sumOf { getScore(it[0], getOwnShape(it[0], it[2])) }
    println("Part 2: $answer2")
}
