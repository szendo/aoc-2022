package com.szendo.aoc.y2022

import com.szendo.aoc.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sign

fun main() {
    val input = Helper.getResourceAsStream("2022/9.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { it[0] to it.substring(2).toInt() }.toList()
        }

    val rope = MutableList(10) { 0 to 0 }
    val visited1 = mutableSetOf<Coord>()
    val visited2 = mutableSetOf<Coord>()

    fun Coord.chebyshevDistance(other: Coord) = max(abs(x - other.x), abs(y - other.y))
    fun Coord.sign() = x.sign to y.sign

    input.forEach { (dir, len) ->
        val delta = when (dir) {
            'U' -> 0 to 1
            'D' -> 0 to -1
            'L' -> -1 to 0
            'R' -> 1 to 0
            else -> error("Invalid direction: $dir")
        }

        repeat(len) {
            rope[0] += delta

            (1 until rope.size).forEach { j ->
                if (rope[j].chebyshevDistance(rope[j - 1]) > 1) {
                    rope[j] += (rope[j - 1] - rope[j]).sign()
                }
            }

            visited1.add(rope[1])
            visited2.add(rope.last())
        }

    }

    val answer1 = visited1.size
    println("Part 1: $answer1")

    val answer2 = visited2.size
    println("Part 2: $answer2")
}
