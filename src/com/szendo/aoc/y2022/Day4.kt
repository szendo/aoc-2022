package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2022/4.txt")
        .bufferedReader()
        .useLines { lines ->
            lines
                .map { line ->
                    line.split(',', limit = 2)
                        .map { range -> range.split('-', limit = 2) }
                        .map { it[0].toInt()..it[1].toInt() }
                }
                .map { it[0] to it[1] }
                .toList()
        }

    val answer1 = run {
        fun IntRange.fullyContains(other: IntRange) = first <= other.first && last >= other.last
        input.count { (r1, r2) -> r1.fullyContains(r2) || r2.fullyContains(r1) }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        fun IntRange.overlaps(other: IntRange) = first <= other.last && last >= other.first
        input.count { (r1, r2) -> r1.overlaps(r2) }
    }
    println("Part 2: $answer2")
}
