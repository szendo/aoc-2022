package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    fun getPriority(c: Char) = if (c.isLowerCase()) c - 'a' + 1 else c - 'A' + 27

    val input = Helper.getResourceAsStream("2022/3.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val answer1 = run {
        input
            .map { it.substring(0 until it.length / 2).toSet().intersect(it.substring(it.length / 2).toSet()).first() }
            .sumOf { getPriority(it) }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.map(String::toSet).chunked(3)
            .map { it[0].intersect(it[1]).intersect(it[2]).first() }
            .sumOf { getPriority(it) }
    }
    println("Part 2: $answer2")
}
