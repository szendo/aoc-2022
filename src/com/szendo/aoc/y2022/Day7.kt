package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2022/7.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val sizes = mutableMapOf<String, Long>()
    val lsInDir = mutableMapOf<String, Int>()

    var currentPath = ""

    input.forEach { line ->
        if (line.startsWith("$ ")) {
            when (val command = line.substring(2)) {
                "ls" -> lsInDir.merge(currentPath, 1, Int::plus)
                "cd /" -> currentPath = ""
                "cd .." -> currentPath = currentPath[0 until currentPath.lastIndexOf('/')]
                else -> currentPath = currentPath + "/" + command.substring(3)
            }
        } else {
            if (lsInDir[currentPath] == 1) {
                val sizeOrDir = line.split(' ', limit = 2)[0]
                if (sizeOrDir != "dir") {
                    sizes.merge(currentPath, sizeOrDir.toLong(), Long::plus)
                }
            }
        }
    }

    val totalSizes = mutableMapOf<String, Long>()
    sizes.keys.forEach { dir ->
        var cdir = dir
        do {
            if (cdir in totalSizes) {
                cdir = cdir[0 until cdir.lastIndexOf('/')]
                continue
            }

            totalSizes[cdir] = sizes.filterKeys { it.startsWith(cdir) }.values.sum()

            if (cdir.isNotEmpty()) {
                cdir = cdir[0 until cdir.lastIndexOf('/')]
            }
        } while (cdir.isNotEmpty())
    }

    val answer1 = totalSizes.values.filter { it < 100_000 }.sum()
    println("Part 1: $answer1")

    val answer2 = (70_000_000 - (totalSizes[""] ?: error("Missing total size for root dir"))).let { free ->
        totalSizes.values.sorted().first { free + it > 30_000_000 }
    }
    println("Part 2: $answer2")
}
