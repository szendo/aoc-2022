package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val trees = Helper.getResourceAsStream("2022/8.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line -> line.map { it.digitToInt() } }.toList()
        }

    val size = trees.size
    val lastIndex = trees.lastIndex

    val top = MutableList(size) { -1 }
    val bottom = MutableList(size) { -1 }
    val left = MutableList(size) { -1 }
    val right = MutableList(size) { -1 }

    val answer1 = run {
        val visible = mutableSetOf<Coord>()

        fun checkTreesFromEdge(c: Coord, isXAcross: Boolean, edge: MutableList<Int>) {
            val across = if (isXAcross) c.x else c.y
            if (trees[c] <= edge[across]) return
            visible.add(c)
            edge[across] = trees[c]
        }

        (0 until size).forEach { inward ->
            (0 until size).forEach { across ->
                checkTreesFromEdge(across to inward, true, top)
                checkTreesFromEdge(across to lastIndex - inward, true, bottom)
                checkTreesFromEdge(inward to across, false, left)
                checkTreesFromEdge(lastIndex - inward to across, false, right)
            }
        }

        visible.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (1 until size).maxOf { y ->
            (1 until size).maxOf { x ->
                val height = trees[x to y]
                listOf((1 to 0), (-1 to 0), (0 to 1), (0 to -1))
                    .map { d ->
                        var k = 1
                        while (
                            when {
                                d.x > 0 -> x + k < lastIndex
                                d.x < 0 -> x - k > 0
                                d.y > 0 -> y + k < lastIndex
                                else -> y - k > 0
                            } && trees[x + d.x * k to y + d.y * k] < height
                        ) {
                            k++
                        }

                        k
                    }
                    .reduce(Int::times)
            }
        }
    }
    println("Part 2: $answer2")
}
