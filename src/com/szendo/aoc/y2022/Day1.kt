package com.szendo.aoc.y2022

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2022/1.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.split("")
                .map { it.map(String::toInt) }
                .toList()
        }

    val answer1 = input.maxOf { it.sum() }
    println("Part 1: $answer1")

    val answer2 = input.map { it.sum() }.sortedDescending().take(3).sum()
    println("Part 2: $answer2")
}
